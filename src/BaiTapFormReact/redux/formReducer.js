import {
  ADD_STUDENT,
  EDIT_STUDENT,
  GET_INFORMATION,
  REMOVE_STUDENT,
  SEARCH_STUDENT,
  UPDATE_STUDENT,
} from "../constants/constants";
import { validate } from "../validate/validate.js";
const initialState = {
  information: {
    maSv: "",
    hoTen: "",
    soDienThoai: "",
    email: "",
  },
  studentList: [],
  editInformation: {},
  searchStudent: [],
};
export const formReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_INFORMATION: {
      state.information = { ...payload };
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case ADD_STUDENT: {
      let cloneList = [...state.studentList];
      let index = cloneList.findIndex((student) => {
        return student.maSv === payload.maSv;
      });
      let isValidMa =
        validate.checkEmpty(payload.maSv, "maErr") &&
        validate.checkMaSo(payload.maSv);
      let isValidHoTen =
        validate.checkEmpty(payload.hoTen, "hoTenErr") &&
        validate.checkName(payload.hoTen);
      let isValidSdt =
        validate.checkEmpty(payload.soDienThoai, "sdtErr") &&
        validate.checkSdt(payload.soDienThoai);
      let isValidEmail =
        validate.checkEmpty(payload.email, "emailErr") &&
        validate.checkEmail(payload.email);
      let isValid = isValidMa && isValidHoTen && isValidSdt && isValidEmail;
      if (index === -1 && isValid === true) {
        cloneList.push(payload);
      }
      state.studentList = cloneList;
      state.information = {
        maSv: "",
        hoTen: "",
        soDienThoai: "",
        email: "",
      };
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case REMOVE_STUDENT: {
      let cloneList = [...state.studentList];
      let index = cloneList.findIndex((student) => {
        return student.maSv === payload;
      });
      cloneList.splice(index, 1);
      state.studentList = cloneList;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case EDIT_STUDENT: {
      state.information = payload;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case UPDATE_STUDENT: {
      let cloneList = [...state.studentList];
      let index = cloneList.findIndex((student) => {
        return student.maSv === payload.maSv;
      });
      let isValidMa =
        validate.checkEmpty(payload.maSv, "maErr") &&
        validate.checkMaSo(payload.maSv);
      let isValidHoTen =
        validate.checkEmpty(payload.hoTen, "hoTenErr") &&
        validate.checkName(payload.hoTen);
      let isValidSdt =
        validate.checkEmpty(payload.soDienThoai, "sdtErr") &&
        validate.checkSdt(payload.soDienThoai);
      let isValidEmail =
        validate.checkEmpty(payload.email, "emailErr") &&
        validate.checkEmail(payload.email);
      let isValid = isValidMa && isValidHoTen && isValidSdt && isValidEmail;
      if (isValid === true) {
        cloneList[index] = payload;
      }
      state.studentList = cloneList;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    case SEARCH_STUDENT: {
      let cloneSearchStudent = [...state.searchStudent];
      let inputValue = document.getElementById("form1").value;
      let userSearch = payload.filter((student) => {
        return student.hoTen.toUpperCase().includes(inputValue.toUpperCase());
      });
      cloneSearchStudent = userSearch;
      state.searchStudent = cloneSearchStudent;
      let json = JSON.stringify(state);
      return { ...JSON.parse(json) };
    }
    default:
      return state;
  }
};
