import React, { Component } from "react";
import GetInformation from "./GetInformation";
import RenderStudentList from "./RenderStudentList";
export default class BaiTapForm extends Component {
  render() {
    return (
      <div className="container">
        <GetInformation />
        <RenderStudentList />
      </div>
    );
  }
}
