import React, { Component } from "react";
import { connect } from "react-redux/es/exports";
import {
  ADD_STUDENT,
  GET_INFORMATION,
  UPDATE_STUDENT,
} from "../constants/constants";
class GetInformation extends Component {
  handleChangeForm = (e) => {
    let name = e.target.name;
    let value = e.target.value;
    let informationStudent = { ...this.props.information, [name]: value };
    this.props.handleGetInformation(informationStudent);
  };
  handleSubmit = (e) => {
    this.props.handleAddStudent(this.props.information);
    e.preventDefault();
  };
  render() {
    return (
      <div className="text-start mt-5">
        <form
          onSubmit={(e) => {
            this.handleSubmit(e);
          }}
          className="row"
          id="studentForm"
        >
          <div className="form-group col-6">
            <label htmlFor="">Mã sinh viên</label>
            <input
              id="maSoSv"
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={
                this.props.information.maSv ||
                this.props.editInformation?.maSv ||
                ""
              }
              type="text"
              className="form-control"
              name="maSv"
              placeholder="Nhập mã sinh viên"
              aria-describedby="helpId"
            />
            <span id="maErr" style={{ fontSize: "20px", color: "red" }}></span>
          </div>{" "}
          <div className="form-group col-6">
            <label htmlFor="">Họ tên sinh viên</label>
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={
                this.props.information.hoTen ||
                this.props.editInformation?.hoTen ||
                ""
              }
              type="text"
              className="form-control"
              name="hoTen"
              placeholder="Nhập họ tên sinh viên"
              aria-describedby="helpId"
              id="hoTenSv"
            />
            <span
              id="hoTenErr"
              style={{ fontSize: "20px", color: "red" }}
            ></span>
          </div>{" "}
          <div className="form-group col-6 mt-2">
            <label htmlFor="">Số điện thoại</label>
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={
                this.props.information.soDienThoai ||
                this.props.editInformation?.soDienThoai ||
                ""
              }
              type="text"
              className="form-control"
              name="soDienThoai"
              placeholder="Nhập số điện thoại"
              aria-describedby="helpId"
            />
            <span style={{ fontSize: "20px", color: "red" }} id="sdtErr"></span>
          </div>{" "}
          <div className="form-group col-6 mt-2">
            <label htmlFor="">Email</label>
            <input
              onChange={(e) => {
                this.handleChangeForm(e);
              }}
              value={
                this.props.information.email ||
                this.props.editInformation?.email ||
                ""
              }
              type="text"
              className="form-control"
              name="email"
              placeholder="Nhập email"
              aria-describedby="helpId"
              id="emailSv"
            />
            <span
              style={{ fontSize: "20px", color: "red" }}
              id="emailErr"
            ></span>
          </div>
          <button className=" btn btn-warning w-25 mt-3 ms-auto">Submit</button>
        </form>
        <div style={{ width: "1200px", paddingLeft: "980px" }}>
          <button
            className="btn btn-info ms-auto mt-3"
            style={{ padding: "7px 137px" }}
            onClick={() => {
              this.props.handleUpdateStudent(this.props.information);
            }}
          >
            Update
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    information: state.formReducer.information,
    studentList: state.formReducer.studentList,
    editInformation: state.formReducer.editInformation,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleGetInformation: (information) => {
      dispatch({
        type: GET_INFORMATION,
        payload: information,
      });
    },
    handleAddStudent: (student) => {
      dispatch({
        type: ADD_STUDENT,
        payload: student,
      });
    },
    handleUpdateStudent: (student) => {
      dispatch({
        type: UPDATE_STUDENT,
        payload: student,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(GetInformation);
