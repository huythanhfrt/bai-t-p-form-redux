import React, { Component } from "react";
import { connect } from "react-redux/es/exports";
import {
  EDIT_STUDENT,
  REMOVE_STUDENT,
  SEARCH_STUDENT,
  UPDATE_STUDENT,
} from "../constants/constants";
class RenderStudentList extends Component {
  render() {
    return (
      <div className="w-100 mt-5">
        <div className="input-group">
          <div className="form-outline">
            <input type="search" id="form1" className="form-control" />
            <label className="form-label" htmlFor="form1">
              Search
            </label>
          </div>
          <button
            type="button"
            className="btn btn-primary"
            style={{ height: "38px" }}
            onClick={() => {
              this.props.handleSearchSv(this.props.studentList);
            }}
          >
            🔍
          </button>
        </div>

        <h2>Danh sách sinh viên</h2>
        <table className="table table-bordered table-hover myTable mt-4">
          <thead>
            <tr>
              <td>Mã</td>
              <td>Họ tên</td>
              <td>Số điện thoại </td>
              <td>Email</td>
              <td>Thao tác</td>
            </tr>
          </thead>
          <tbody>
            {this.props.studentList.map((student) => {
              if (this.props.studentList.length > 0) {
                return (
                  <tr>
                    <td>{student.maSv}</td>
                    <td>{student.hoTen}</td>
                    <td>{student.soDienThoai}</td>
                    <td>{student.email}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleEditStudent(student);
                        }}
                        className="btn btn-success"
                      >
                        Sửa
                      </button>
                      <button
                        onClick={() => {
                          this.props.handleRemoveStudent(student.maSv);
                        }}
                        className="btn btn-danger"
                      >
                        Xóa
                      </button>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </table>
        <h2>Sinh viên bạn tìm kiếm</h2>
        <table className="table table-bordered table-hover myTable mt-4">
          <thead>
            <tr>
              <td>Mã</td>
              <td>Họ tên</td>
              <td>Số điện thoại </td>
              <td>Email</td>
            </tr>
          </thead>
          <tbody>
            {this.props.searchStudent.map((student) => {
              return (
                <tr>
                  <td>{student.maSv}</td>
                  <td>{student.hoTen}</td>
                  <td>{student.soDienThoai}</td>
                  <td>{student.email}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    studentList: state.formReducer.studentList,
    searchStudent: state.formReducer.searchStudent,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    handleRemoveStudent: (maSv) => {
      dispatch({
        type: REMOVE_STUDENT,
        payload: maSv,
      });
    },
    handleEditStudent: (student) => {
      dispatch({
        type: EDIT_STUDENT,
        payload: student,
      });
    },
    handleSearchSv: (studentList) => {
      dispatch({
        type: SEARCH_STUDENT,
        payload: studentList,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(RenderStudentList);
