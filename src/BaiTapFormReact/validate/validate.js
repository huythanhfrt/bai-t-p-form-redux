export const validate = {
  checkMaSo: (maSv) => {
    let parten = /^[0-9]*$/;
    if (parten.test(maSv)) {
      document.getElementById("maErr").style.display = "block";
      document.getElementById("maErr").innerText = "";
      return true;
    }
    document.getElementById("maErr").style.display = "block";
    document.getElementById("maErr").innerText =
      "Vui lòng nhập đúng định dạng ( chỉ nhập số )";
    return false;
  },
  checkSdt: (sdt) => {
    let parten = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    if (parten.test(sdt)) {
      document.getElementById("sdtErr").style.display = "block";
      document.getElementById("sdtErr").innerText = "";
      return true;
    }
    document.getElementById("sdtErr").style.display = "block";
    document.getElementById("sdtErr").innerText =
      "Vui lòng nhập đúng định dạng";
    return false;
  },
  checkName: (name) => {
    let parten =
      /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    if (parten.test(name)) {
      document.getElementById("hoTenErr").style.display = "block";
      document.getElementById("hoTenErr").innerText = "";
      return true;
    }
    document.getElementById("hoTenErr").style.display = "block";
    document.getElementById("hoTenErr").innerText = "Nhập sai định dạng ";
    return false;
  },
  checkEmail: (email) => {
    let parten = /^[a-z0-9](\.?[a-z0-9]){5,}@g(oogle)?mail\.com$/;
    if (parten.test(email)) {
      document.getElementById("emailErr").style.display = "block";
      document.getElementById("emailErr").innerText = "";
      return true;
    }
    document.getElementById("emailErr").style.display = "block";
    document.getElementById("emailErr").innerText = "Nhập sai định dạng";
    return false;
  },
  checkEmpty: (value, idError) => {
    if (value == "") {
      document.getElementById(idError).style.display = "block";
      document.getElementById(idError).innerText = "Không được để rỗng";
      return false;
    }
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "";
    return true;
  },
};
