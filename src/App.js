import logo from "./logo.svg";
import "./App.css";
import BaiTapForm from "./BaiTapFormReact/components/BaiTapForm";

function App() {
  return (
    <div className="App">
      <BaiTapForm />
    </div>
  );
}

export default App;
